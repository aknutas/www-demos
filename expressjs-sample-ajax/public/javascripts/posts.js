if (document.readyState !== "loading") {
  // Document ready, executing
  console.log("Document ready, executing");
  initializeCode();
} else {
  document.addEventListener("DOMContentLoaded", function() {
    // Document was not ready, executing when loaded
    console.log("Document ready, executing after a wait");
    initializeCode();
  });
}

function initializeCode() {
  console.log("Initializing");
  loadAjax();
}

// See https://www.npmjs.com/package/superagent
function loadAjax() {
  superagent
    .get("http://localhost:3000/posts/")
    .set('Accept', 'application/json')
    .then(res => {
      // res.body, res.headers, res.status
      console.log(res.status);
      console.log(res.body);
      // Neat! Superagent automatically parsed the JSON data
      document.getElementById("posts").innerHTML = "";
      for (const item of res.body) {
          document.getElementById("posts").innerHTML = "".concat(
          document.getElementById("posts").innerHTML,
          "<li>",
          item.author,
          ": ",
          item.content,
          "</li>"
        );
      }
      window.setTimeout(loadAjax, 5000);
    })
    .catch(err => {
      // err.message, err.response
      console.log("ERROR");
      console.log(err.message);
      console.log(err.response);
      window.setTimeout(loadAjax, 5000);
    });
}
